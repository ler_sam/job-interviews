from sortedcontainers import SortedSet


def calculate_time_window(values: list, window_size: float) -> list:
    """
    calculate time window without repeating values
    @param values: floating nuber range, exp: [0.1, 0.5, 1.2, 1.8, 2.5, 3.1, 4.0, 4.5, 5.5, 6.0]
    @param window_size: floating window range value, exp: 1.0
    @return: list of range, exp:  [[0.1, 0.5, 1.2], [1.2, 1.8, 2.5], [2.5, 3.1, 4.0], [4.0, 4.5, 5.5]]
    """
    start_time = end_time = values[0]
    result = []
    _slice = SortedSet()
    for value in values:
        end_time = value
        _slice.add(value)
        if end_time - start_time >= window_size:
            result.append(list(_slice))
            _slice.clear()
            _slice.add(value)
            start_time = value

    # Append the final time window
    if end_time - start_time >= window_size:
        result.append((start_time, end_time))
    return result

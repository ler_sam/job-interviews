import pytest

from leetcode.number_manipulation.palindrome_number import solution


@pytest.mark.PalindromeNumber
@pytest.mark.parametrize("actual, expected", [
    (121, True),
    (-121, False),
    (10, False),
    (-101, False),
    (1221, True),
    (12121, True),
    (123121, False),
])
def test_palindrome_number(actual, expected):
    assert solution(actual) == expected

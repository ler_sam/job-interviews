import pytest

from leetcode.arrays.merge_intervals import solution


@pytest.mark.MergeIntervals
@pytest.mark.parametrize("actual, expected", [
    ([[1, 3], [2, 6], [8, 10], [15, 18]], [[1, 6], [8, 10], [15, 18]]),
    ([[1, 4], [4, 5]], [[1, 5]]),
    ([[4, 5], [1, 4]], [[1, 5]]),
    ([[1, 4], [2, 3]], [[1, 4]])
])
def test_merge_intervals(actual, expected):
    assert solution(actual) == expected

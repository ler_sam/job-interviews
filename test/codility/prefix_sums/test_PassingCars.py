import pytest
from codility.prefix_sums.passing_cars import solution


# https://app.codility.com/programmers/lessons/5-prefix_sums/passing_cars/

@pytest.mark.PassingCars
@pytest.mark.parametrize("actual, expected", [
    ([0, 1, 0, 1, 1], 5),
    ([0, 1, 0, 1, 0], 3),
])
def test_PassingCars(actual, expected):
    assert solution(actual) == expected

"""
https://www.geeksforgeeks.org/check-whether-two-strings-are-anagram-of-each-other/

Write a function to check whether two given strings are anagram of each other or not.
An anagram of a string is another string that contains the same characters,
only the order of characters can be different.

For example, "abcd" and "dabc" are an anagram of each other.
"""

# def is_anagram(s, t):
#     '''
#     more expensive because of sorting
#     '''
#     if s == t or len(s) != len(t):
#         return False

#     s = sorted(s)
#     t = sorted(t)
#     return s == t

NO_OF_CHARS = 256


def is_anagram(s, t):
    count_array = [0] * NO_OF_CHARS

    if s == t or len(s) != len(t):
        return False

    for char in s:
        count_array[ord(char)] += 1

    for char in t:
        count_array[ord(char)] -= 1
        if count_array[ord(char)] < 0:
            return False

    return True

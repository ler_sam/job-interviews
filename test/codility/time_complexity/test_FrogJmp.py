import pytest

from codility.time_complexity.frog_jmp import solution

# https://app.codility.com/programmers/lessons/3-time_complexity/frog_jmp/


@pytest.mark.FrogJmp
@pytest.mark.parametrize("X, Y, D, R", [
    (10, 85, 30, 3),
    (10, 300, 40, 8)
    ])
def test_FrogJmp(X, Y, D, R):
    assert solution(X, Y, D) == R
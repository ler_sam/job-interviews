import pytest

from codility.counting_elements.frog_river_one import solution

# https://app.codility.com/programmers/lessons/4-counting_elements/frog_river_one/


@pytest.mark.FrogRiverOne
@pytest.mark.parametrize("input_array, position, result", [
    ([1, 3, 1, 4, 2,  3, 5, 4], 5, 6),
    ])
def test_FrogRiverOne (input_array, position, result):
    assert solution (position, input_array) == result

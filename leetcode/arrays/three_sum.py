# https://leetcode.com/problems/3sum/

def solution(nums: list) -> list:
    """
    Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k,
    and j != k, and nums[i] + nums[j] + nums[k] == 0.

    Notice that the solution set must not contain duplicate triplets.
    :type nums: list[int]
    :rtype list[list[int]]:
    """
    nums.sort()

    caching = {}
    for i in range(len(nums) - 2):
        temp_sum = 0 - nums[i]
        low = i + 1
        high = len(nums) - 1
        while low < high:
            two_sum = nums[low] + nums[high]
            if two_sum == temp_sum:
                caching[(nums[i], nums[low], nums[high])] = 1
                low += 1
                high -= 1
            elif two_sum > temp_sum:
                high -= 1
            else:
                low += 1
    return [list(item) for item in caching.keys()]

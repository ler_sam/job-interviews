import pytest

from codility.iterations.binary_gap import solution

# https://app.codility.com/programmers/lessons/1-iterations/binary_gap/


@pytest.mark.BinaryGap
@pytest.mark.parametrize("value, gap", [
    (9, 2),
    (529, 4),
    (20, 1),
    (15, 0),
    (32,0)
    ])
def test_binary_gap(value, gap):
    assert solution(value) == gap

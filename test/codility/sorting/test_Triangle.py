import pytest

from codility.sorting.triangle import solution

# https://app.codility.com/programmers/lessons/6-sorting/triangle/


@pytest.mark.Triangle
@pytest.mark.parametrize("input_array, result", [
    ([10, 2, 5, 1, 8, 20], 1),
    ([10, 50, 5, 1], 0)
])
def test_Triangle(input_array, result):
    assert solution(input_array) == result

from random import randint


def permutations(input_number):
    """
    create permutations array
    O(N)
    main idea, shuffle implementation
    """
    x = []
    for i in range(input_number):
        x.append(i + 1)

    for i in range(input_number):
        t = randint(0, input_number)
        x[i], x[t] = x[t], x[i]

    return x


if __name__ == '__main__':
    print(permutations(4))

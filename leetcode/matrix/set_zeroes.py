# https://leetcode.com/problems/set-matrix-zeroes/

def solution(matrix: list) -> None:
    """
    73. Set Matrix Zeroes
    Given an m x n integer matrix matrix, if an element is 0, set its entire row and column to 0's,
    and return the matrix.

    You must do it in place.
    :type matrix: list[list[int]]
    """

    zero_row = set()
    zero_col = set()

    # get position of Zeroes in matrix
    for row in range(len(matrix)):
        for col in range(len(matrix[0])):
            if matrix[row][col] == 0:
                zero_row.add(row)
                zero_col.add(col)

    # reset matrix rows
    for row in zero_row:
        for col in range(len(matrix[0])):
            matrix[row][col] = 0

    # reset matrix columns
    for col in zero_col:
        for row in range(len(matrix)):
            matrix[row][col] = 0

import pytest
from leetcode.matrix.set_zeroes import solution


@pytest.mark.SetZeroes
@pytest.mark.parametrize("actual, expected", [
    ([[1, 1, 1], [1, 0, 1], [1, 1, 1]], [[1, 0, 1], [0, 0, 0], [1, 0, 1]]),
    ([[0, 1, 2, 0], [3, 4, 5, 2], [1, 3, 1, 5]], [[0, 0, 0, 0], [0, 4, 5, 0], [0, 3, 1, 0]])
])
def test_set_matrix_zeroes(actual, expected):
    solution(actual)
    assert actual == expected

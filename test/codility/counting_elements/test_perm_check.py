import random
import pytest

from codility.counting_elements.perm_check import solution

# https://app.codility.com/programmers/lessons/4-counting_elements/perm_check/


@pytest.mark.PermCheck
@pytest.mark.parametrize("input_array, permutation", [
    ([4, 1, 3, 2], 1),
    ([4, 1, 3], 0),
    ([], 0),
    ([1], 1),
    ([2], 0),
    ([1, 2], 1),
    ([2, 1], 1),
    ([2, 2], 0),
    ([1, 1], 0),
    ([1, 2, 3], 1),
    ([3, 2, 1], 1),
    ([5, 2, 1], 0),
    ([3, 1], 0),
    ([3, 3, 3, 3, 2, 1], 0),
    ([3, 3, 3, 3, 3, 3], 0),
])
def test_PermCheck(input_array, permutation):
    assert solution(input_array) == permutation


ARR_RANGE = (1, 100000)


def test_PermCheck_extreme():
    # full complement of numbers
    arr = list(range(*ARR_RANGE))
    random.shuffle(arr)
    assert solution(arr) == 1

    # full complement with one missing
    arr.remove(random.randint(1, len(arr)))
    assert solution(arr) == 0

import pytest

from codility.sorting.max_product_of_three import solution

# https://app.codility.com/programmers/lessons/6-sorting/max_product_of_three/


@pytest.mark.MaxProductOfThree
@pytest.mark.parametrize("input_array, result", [
        ([-3, 1, 2, -2, 5, 6], 60),
        ([-5, 5, -5, 4], 125),
        ([0, 0, 0, 0], 0),
        ([-10, -10, -3, 0, 1], 100),
        ([-4, -3, -2, -1], -6),
        ([-1, -1, 1, 2], 2),
        ([-5, -5, 1, 2], 50),
        ([-5, -5, -1, 0], 0),
])
def test_MaxProductOfThree(input_array, result):
    assert solution(input_array) == result

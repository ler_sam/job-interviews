# https://leetcode.com/problems/jump-game/

def solution(nums: list) -> bool:
    """
    You are given an integer array nums.
    You are initially positioned at the array's first index,
    and each element in the array represents your maximum jump length at that position.

    Return true if you can reach the last index, or false otherwise.

    :type nums: list[int]
    """

    last_good_index = len(nums) - 1
    for i in range(last_good_index, -1, -1):
        if i + nums[i] >= last_good_index:
            last_good_index = i

    return last_good_index == 0

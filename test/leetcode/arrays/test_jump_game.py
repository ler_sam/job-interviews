import pytest

from leetcode.arrays.jump_game import solution


# https://leetcode.com/problems/jump-game/
# https://www.youtube.com/watch?v=Zb4eRjuPHbM&ab_channel=NickWhite

@pytest.mark.JumpGame
@pytest.mark.parametrize("actual, expected", [
    ([2, 3, 1, 1, 4], True),
    ([3, 2, 1, 0, 4], False),
    ([2, 5, 0, 0], True),
    ([0], True)

])
def test_jump_game(actual, expected):
    assert solution(actual) == expected

import functools


@functools.lru_cache(maxsize=128, typed=False)
def caching_recursive_solution(n: int) -> int:
    '''
    recursion caching
    '''
    if n <= 2:
        return 1

    return caching_recursive_solution(n - 1) + caching_recursive_solution(n - 2)


def caching_loop_solution(n: int) -> int:
    '''
    bottom-up
    '''
    caching = [0, 1, 1, 2]
    if n in caching:
        return caching[n]

    for k in range(len(caching), n + 1):
        caching.append(caching[k - 1] + caching[k - 2])

    return caching[n]

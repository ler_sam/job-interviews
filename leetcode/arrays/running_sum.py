# https://leetcode.com/problems/running-sum-of-1d-array/

def my_solution(nums: list) -> list:
    """
    Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
    Return the running sum of nums.

    :type nums: List[int]
    :rtype: List[int]
    """
    temp = 0

    for i in range(len(nums)):
        temp += nums[i]
        nums[i] = temp

    return nums


def best_solution(nums: list) -> list:
    """
    Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
    Return the running sum of nums.

    :type nums: List[int]
    :rtype: List[int]
    """
    for i in range(1, len(nums)):
        nums[i] += nums[i - 1]

    return nums

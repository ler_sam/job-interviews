# https://app.codility.com/programmers/lessons/6-sorting/max_product_of_three/

def solution(A):
    A.sort()
    first_product = A[0] * A[1] * A[-1]  # max negative * 2 index 0, 1; max positive index -1
    last_product = A[-1] * A[-2] * A[-3]  # all positive

    return max(first_product, last_product)

import pytest

from codility.prefix_sums.genomic_range_query import solution

# https://app.codility.com/programmers/lessons/5-prefix_sums/genomic_range_query/

@pytest.mark.GenomicRangeQuery
@pytest.mark.parametrize("S, P, Q, result", [
    ('CAGCCTA', [2,5,0], [4,5,6], [2,4,1]),
])
def test_GenomicRangeQuery(S, P, Q, result):
    assert solution(S, P, Q) == result

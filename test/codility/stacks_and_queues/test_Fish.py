import pytest

# https://app.codility.com/programmers/lessons/7-stacks_and_queues/fish/

from codility.stacks_and_queues.fish import solution


@pytest.mark.Fish
@pytest.mark.parametrize("A, B, result", [
    ([4, 3, 2, 1, 5], [0, 1, 0, 0, 0], 2),
    ([4, 3, 2, 1, 5], [1, 0, 1, 0, 1], 3),
    ([4, 3, 2, 0, 5], [0, 1, 0, 0, 0], 2),
    ([4, 3, 2, 1, 5], [0, 1, 0, 0, 0], 2),
    ([4, 3, 2, 1, 5], [0, 1, 1, 0, 0], 2),
    ([4, 3, 2, 5, 6], [1, 0, 1, 0, 1], 2),
    ([7, 4, 3, 2, 5, 6], [0, 1, 1, 1, 0, 1], 3),
    ([3, 4, 2, 1, 5], [1, 0, 0, 0, 0], 4),
    ([3], [1], 1),
    ([3], [0], 1),
])
def test_fish(A, B, result):
    assert solution(A, B) == result

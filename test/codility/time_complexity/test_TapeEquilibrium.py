import pytest

from codility.time_complexity.tape_equilibrium import solution

# https://app.codility.com/programmers/lessons/3-time_complexity/tape_equilibrium/


@pytest.mark.TapeEquilibrium
@pytest.mark.parametrize("input_array, equilibrum", [
    ([3,1,2,4,3], 1),
    ([13, 18, 5, 36, 15, 4, 3], 22),
    ([5, 27, 5, 44, 6, 20, 31], 24)
    ])
def test_TapeEquilibrium(input_array, equilibrum):
    assert solution(input_array) == equilibrum


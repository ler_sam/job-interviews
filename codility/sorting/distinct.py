# https://app.codility.com/programmers/lessons/6-sorting/distinct/

def solution(A):
    """
    Task Score:     100%
    Correctness:    100%
    Performance:    100%
    """
    temp_stuck = {}
    for val in A:
        temp_stuck[val] = val

    return len(temp_stuck.keys())

# https://leetcode.com/problems/remove-duplicates-from-sorted-list/
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def remove_duplicates(head: Optional[ListNode]) -> Optional[ListNode]:
    """
    Given the head of a sorted linked list, delete all duplicates such that each element appears only once.
    Return the linked list sorted as well.

    @param head:
    @return:
    """
    if not head:
        return head

    current = head
    runner = head.next
    while runner is not None:
        if current.val == runner.val:
            current.next = runner.next
        else:
            current = runner

        runner = runner.next
    return head

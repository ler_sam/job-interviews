# https://app.codility.com/programmers/lessons/4-counting_elements/perm_check/

# def solution(A):
#     '''
#         Task Score:  75%
#         Correctness: 83%
#         Performance: 66%
#     '''
#     result = 0
#     n = len(A)
#     total_sum = n * (n+1) // 2
#     in_sum = sum(A)

#     if total_sum == in_sum and n > 0:
#         result = 1
#     return result

def solution(A):
    """
    :param A: a list of integers
    :return: true if the list is a permutation (sequence from 1 to N)
    """
    # an empty list is not a permutation
    if len(A) == 0:
        return 0

    # track the hits with a dictionary
    hits = {}
    for item in A:
        # (quick exit) each element once and only once thanks
        if item in hits:
            return 0
        hits[item] = True

    # (quick exit again) each element once and only once
    if len(hits) != len(A):
        return 0

    # ok, see if they're all there
    for num in range(1, len(A) + 1):
        if num not in hits:
            return 0

    return 1

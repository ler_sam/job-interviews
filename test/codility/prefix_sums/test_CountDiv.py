import pytest

from codility.prefix_sums.count_div import solution

# https://app.codility.com/programmers/lessons/5-prefix_sums/count_div/

# https://www.geeksforgeeks.org/count-numbers-divisible-m-given-range/
@pytest.mark.CountDiv
@pytest.mark.parametrize("A, B, K, result", [
    (25, 100, 30, 3),
    (6, 15, 3, 4),
    (6, 11, 2, 3),
])
def test_CountDiv(A, B, K, result):
    assert solution(A, B, K) == result

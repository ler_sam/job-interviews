import pytest
from codility.interview.fibonacci import caching_loop_solution, caching_recursive_solution


@pytest.mark.Fibonacci
@pytest.mark.parametrize("N, result", [
    (6, 8),
    (7, 13),
    (8, 21),
])
def test_fibonacci_loop_solution(N, result) -> None:
    assert caching_loop_solution(N) == result


@pytest.mark.Fibonacci
@pytest.mark.parametrize("N, result", [
    (6, 8),
    (7, 13),
    (8, 21),
])
def test_fibonacci_recursive_solution(N, result) -> None:
    assert caching_recursive_solution(N) == result

# https://app.codility.com/programmers/lessons/4-counting_elements/missing_integer/

def solution(A):
    min_missing = 1
    for val in sorted(A):
        if val == min_missing:
            min_missing += 1

        if val > min_missing:
            break

    return min_missing

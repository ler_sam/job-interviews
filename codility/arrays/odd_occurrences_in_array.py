# https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/

def solution(A):
    result_set = set()
    for val in A:
        if val in result_set:
            result_set.discard(val)
        else:
            result_set.add(val)

    return result_set.pop()

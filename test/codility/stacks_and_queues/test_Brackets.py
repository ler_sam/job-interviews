import pytest

# https://app.codility.com/programmers/lessons/7-stacks_and_queues/brackets/

from codility.stacks_and_queues.brackets import solution

@pytest.mark.Brackets
@pytest.mark.parametrize("str1, status", [
    ("[()()]", 1), ("[(]()]", 0),
    ("[{()}]", 1), ("][", 0),
    ('([)()]', 0)
    ])

def test_Brackets(str1, status):
    assert solution(str1) == status
import pytest

from codility.arrays.cyclic_rotation import solution


@pytest.mark.CyclicRotation
@pytest.mark.parametrize("input_array, rotation, result", [
    ([5, 3, 4, 1, 2], 2, [1, 2, 5, 3, 4]),
    ([1, 2, 3, 4, 5], 2, [4, 5, 1, 2, 3]),
    ([1, 2, 3, 4, 5], 5, [1, 2, 3, 4, 5]),
    ([3, 8, 9, 7, 6], 3, [9, 7, 6, 3, 8]),
    ([-1, -2, -3, -4], 2, [-3, -4, -1, -2])
])
def test_cyclic_rotation(input_array, rotation, result):
    assert solution(input_array, rotation) == result

# https://app.codility.com/programmers/lessons/5-prefix_sums/count_div/

def solution(A, B, K):
    result = (B // K) - (A // K)
    # Add 1 explicitly as A is divisible by K
    if A % K == 0:
        result += 1

    # A is not divisible by K
    return result

import pytest

from leetcode.arrays.two_sum import solution


@pytest.mark.TwoSum
@pytest.mark.parametrize("actual, expected", [
    ({'nums': [2, 7, 11, 15], 'target': 9}, [0, 1]),
    ({'nums': [3, 2, 4], 'target': 6}, [1, 2]),
    ({'nums': [3, 3], 'target': 6}, [0, 1]),
])
def test_find_difference(actual, expected):
    assert solution(**actual) == expected

import pytest

from leetcode.arrays.running_sum import my_solution
from leetcode.arrays.running_sum import best_solution


@pytest.mark.RunningSum
@pytest.mark.parametrize("actual, expected", [
    ([1, 2, 3, 4], [1, 3, 6, 10]),
    ([1, 1, 1, 1, 1], [1, 2, 3, 4, 5]),
    ([3, 1, 2, 10, 1], [3, 4, 6, 16, 17])
])
def test_running_sum_my(actual, expected):
    assert my_solution(actual) == expected


@pytest.mark.RunningSum
@pytest.mark.parametrize("actual, expected", [
    ([1, 2, 3, 4], [1, 3, 6, 10]),
    ([1, 1, 1, 1, 1], [1, 2, 3, 4, 5]),
    ([3, 1, 2, 10, 1], [3, 4, 6, 16, 17])
])
def test_running_sum_best(actual, expected):
    assert best_solution(actual) == expected

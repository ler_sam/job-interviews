import pytest

from codility.counting_elements.max_counters import solution

# https://app.codility.com/programmers/lessons/4-counting_elements/max_counters/


@pytest.mark.MaxCounters
@pytest.mark.parametrize("input_array, counters, result_array", [
    ([3, 4, 4, 6, 1, 4, 4], 5, [3, 2, 2, 4, 2]),
    ])
def test_MaxCounters (input_array, counters, result_array):
    assert solution (counters, input_array) == result_array
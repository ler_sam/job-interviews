# https://app.codility.com/programmers/lessons/7-stacks_and_queues/brackets/

def solution(S):
    """
    Task Score:     100%
    Correctness:    100%
    Performance:    100%
    """
    check_stack = []
    try:
        for chart in S:
            if chart in ['[', '(', '{']:
                check_stack.append(chart)
            else:
                if check_stack:
                    top_char = check_stack[-1]

                if chart == ']' and top_char != '[':
                    return 0

                if chart == ')' and top_char != '(':
                    return 0

                if chart == '}' and top_char != '{':
                    return 0

                check_stack.pop()
    except Exception:
        return 0

    if len(check_stack) == 0:
        return 1

    return 0

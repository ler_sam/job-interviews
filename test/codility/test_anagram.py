import pytest

from codility.valid_anagram import is_anagram


@pytest.mark.parametrize("str1, str2, status", [
    ("geeksforgeeks", "forgeeksgeeks", True),
    ("action man", "cannot aim", True),
    ("action xan", "cannot aim", False)])
def test_is_anagram(str1, str2, status):
    assert is_anagram(str1, str2) == status

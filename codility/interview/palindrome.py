def solution(input_str):
    palindrome_status = True
    for index in range(len(input_str)):
        if input_str[index] != input_str[-1 - index]:
            palindrome_status = False
            break

    return palindrome_status

# https://app.codility.com/programmers/lessons/5-prefix_sums/passing_cars/

def solution(A):
    """
    Array A contains only 0s and/or 1s:
        0 represents a car traveling east,
        1 represents a car traveling west.
    """
    se = 0
    s = 0
    for value in A:
        if value == 0:
            se += 1
        elif value == 1:
            s += se

    if s > 1000000000:
        return -1

    return s

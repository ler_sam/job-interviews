# https://app.codility.com/programmers/lessons/3-time_complexity/frog_jmp/

def solution(X, Y, D):
    result = (Y - X) // D
    if (Y - X) % D != 0:
        result += 1

    return result

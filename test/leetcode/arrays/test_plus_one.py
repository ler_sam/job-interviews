import pytest
from leetcode.arrays.plus_one import solution


# https://leetcode.com/problems/plus-one/


@pytest.mark.plusOne
@pytest.mark.parametrize("actual, expected", [
    ([1, 2, 3], [1, 2, 4]),
    ([4, 3, 2, 1], [4, 3, 2, 2]),
    ([9], [1, 0]),
    ([2,9,9], [3, 0, 0])
])
def test_plus_one(actual, expected):
    assert expected == solution(actual)

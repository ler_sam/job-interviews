import pytest

from leetcode.strings.longest_substring import solution


@pytest.mark.LongestSubstring
@pytest.mark.parametrize("actual, expected", [
    ('abcabcbb', 3),
    ('bbbbb', 1),
    ('pwwkew', 3),
    ('', 0),
    ('aabaab!bb', 3),
    ('dvdf', 3),
])
def test_find_difference(actual, expected):
    assert solution(actual) == expected

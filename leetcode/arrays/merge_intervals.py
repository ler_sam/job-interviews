# https://leetcode.com/problems/merge-intervals/


def solution(intervals: list) -> list:
    """
    Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
    and return an array of the non-overlapping intervals that cover all the intervals in the input.

    :type intervals: list[list[int]]
    :rtype list[list[int]]
    """
    intervals = sorted(intervals, key=lambda x: x[0])
    last_index = 0
    result_data = [intervals[0]]
    for item in range(1, len(intervals), 1):
        start, end = intervals[item]
        if start <= result_data[last_index][1]:
            if end >= result_data[last_index][1]:
                result_data[last_index][1] = end
        else:
            result_data.append(intervals[item])
            last_index += 1
    return result_data

def solution(A):
    n = len(A) + 1
    global_sum = n * (n + 1) // 2  # sum index

    curr_sum = sum(A)

    return global_sum - curr_sum

import pytest
from typing import Optional
from leetcode.sorted_list.remove_duplicates import (remove_duplicates, ListNode)


def create_input_list(data: []) -> Optional[ListNode]:
    runner = head = ListNode()
    for index, item in enumerate(data):
        runner.val = item
        if index < len(data) - 1:
            runner.next = ListNode()
            runner = runner.next
        else:
            runner.next = None
    return head


def convert_actual_to_list(head: Optional[ListNode]) -> list:
    data = []
    runner = head
    while runner is not None:
        data.append(runner.val)
        runner = runner.next
    return data


@pytest.mark.RemoveDuplicates
@pytest.mark.parametrize("input, expected", [
    ([], [0]),
    ([1, 1, 1], [1]),
    ([1, 1, 2], [1, 2]),
    ([1, 1, 2, 3, 3], [1, 2, 3]),
])
def test_remove_duplicates(input, expected):
    head = create_input_list(input)
    actual_list = remove_duplicates(head)
    actual = convert_actual_to_list(actual_list)

    assert actual == expected

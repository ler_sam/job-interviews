import pytest

from codility.interview.find_pivot_index import solution


@pytest.mark.FindPivotIndex
@pytest.mark.parametrize("input_array, pivot_index", [
    ([1, 7, 3, 6, 5, 6], 3),
    ([1, 2, 3], -1),
    ([2, 1, -1], 0),
    ([-1, -1, 0, 1, 1, 0], 5),
])
def test_FindPivotIndex(input_array, pivot_index):
    assert solution(input_array) == pivot_index

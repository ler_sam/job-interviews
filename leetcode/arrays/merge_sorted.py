# https://leetcode.com/problems/merge-sorted-array/
from typing import List


def merge_sorted(nums1: List[int], m: int, nums2: List[int], n: int) -> None:
    """
    Do not return anything, modify nums1 in-place instead.
    """
    first_list = nums1[0:m]
    second_list = nums2[0:n]
    curr_index = 0
    first_index = second_index = 0
    while first_index < m and second_index < n:
        if first_list[first_index] < second_list[second_index]:
            nums1[curr_index] = first_list[first_index]
            first_index += 1
        else:
            nums1[curr_index] = second_list[second_index]
            second_index += 1
        curr_index += 1

    for i in range(first_index, m):
        nums1[curr_index] = first_list[i]
        curr_index += 1

    for i in range(second_index, n):
        nums1[curr_index] = second_list[i]
        curr_index += 1

# https://app.codility.com/programmers/lessons/3-time_complexity/tape_equilibrium/

def solution(A):
    chop_1 = A[0]
    chop_2 = sum(A[1:])
    best = abs(chop_1 - chop_2)

    for val in A[1:-1]:
        chop_1 += val
        chop_2 -= val
        current_diff = abs(chop_1 - chop_2)
        if current_diff < best:
            best = current_diff

    return best

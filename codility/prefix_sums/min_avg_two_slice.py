# https://app.codility.com/programmers/lessons/5-prefix_sums/min_avg_two_slice/

def solution(A):
    min_idx = 0
    min_value = (A[0] + A[1]) / 2.0

    for idx in range(1, len(A) - 1):
        if (A[idx] + A[idx + 1]) / 2.0 < min_value:
            min_idx = idx
            min_value = (A[idx] + A[idx + 1]) / 2.0
        if idx < len(A) - 2 and (A[idx] + A[idx + 1] + A[idx + 2]) / 3.0 < min_value:
            min_idx = idx
            min_value = (A[idx] + A[idx + 1] + A[idx + 2]) / 3.0

    return min_idx

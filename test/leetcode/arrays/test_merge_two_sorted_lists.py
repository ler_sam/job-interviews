from typing import Optional

import pytest
from leetcode.arrays.merge_two_sorted_lists import (merge_two_lists, ListNode)


@pytest.fixture(params=[
    ([1, 2, 4], [1, 3, 4], [1, 1, 2, 3, 4, 4]),
    ([], [], []),
    ([0], [], [0])
])
def input_params(request):
    list1, list2, expected = request.param
    list_1 = ListNode()
    current = list_1
    for item in list1:
        current.next = ListNode(val=item)
        current = current.next
    list_1 = list_1.next

    list_2 = ListNode()
    current = list_2
    for item in list2:
        current.next = ListNode(val=item)
        current = current.next
    list_2 = list_2.next
    return list_1, list_2, expected


def collect_actual(actual: Optional[ListNode]) -> list:
    result =[]
    while actual:
        result.append(actual.val)
        actual = actual.next
    return result


@pytest.mark.MergeTwoSortedLists
def test_merge_two_sorted_lists(input_params):
    list1, list2, expected = input_params

    actual = merge_two_lists(list1, list2)
    actual = collect_actual(actual)

    assert actual == expected
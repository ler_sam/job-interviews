# https://leetcode.com/problems/two-sum/

def solution(nums: list, target: int) -> list:
    """
    Given an array of integers nums and an integer target,
    return indices of the two numbers such that they add up to target.
    You may assume that each input would have exactly one solution, and you may not use the same element twice.
    You can return the answer in any order.

    :type nums: list[int]
    :type target: int
    :rtype list[int]
    """
    caching_dict = {}
    for i, val in enumerate(nums):
        if val not in caching_dict:
            caching_dict[val] = []
        caching_dict[val].append(i)

    for index, check in enumerate(nums):
        dif = target - check

        if dif in caching_dict:
            if check == dif:
                if len(caching_dict[dif]) > 1:
                    return caching_dict[dif]
            else:
                return [index, *caching_dict[dif]]
    return []

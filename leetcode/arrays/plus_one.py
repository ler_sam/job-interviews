# https://leetcode.com/problems/plus-one/
from typing import List


def solution(digits: List[int]) -> List[int]:
    """
    You are given a large integer represented as an integer array digits,
    where each digits[i] is the ith digit of the integer.
    The digits are ordered from most significantt to least significant in left-to-right order.
    The large integer does not contain any leading 0's.

    Increment the large integer by one and return the resulting array of digits.
    """

    remainder = 1
    for i in range(len(digits) - 1, -1, -1):
        curr = digits[i] + remainder

        remainder = 1 if curr // 10 == 1 else 0
        digits[i] = curr % 10

    if remainder > 0:
        digits.insert(0, remainder)

    return digits

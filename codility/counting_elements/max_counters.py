# https://app.codility.com/programmers/lessons/4-counting_elements/max_counters/

def solution(N, A):
    '''
    Task Score:  88%
    Correctness: 100%
    Performance: 80%

    https://app.codility.com/demo/results/trainingA7DJUR-536/
    '''
    result_array = [0] * N
    max_counter = 0

    for val in A:
        if val <= N:
            result_array[val - 1] += 1
            curr_val = result_array[val - 1]
            if curr_val > max_counter:
                max_counter = curr_val
        else:
            result_array = [max_counter] * N
    return result_array

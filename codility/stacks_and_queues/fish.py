# https://app.codility.com/programmers/lessons/7-stacks_and_queues/fish/

def solution(A, B):
    stack = []
    survivors = 0
    for i, weight in enumerate(A):  # O(n)
        if B[i] == 1:
            stack.append(weight)
        else:
            weight_down = stack.pop() if stack else -1

            while weight_down != -1 and weight_down < weight:  # O(m)
                weight_down = stack.pop() if stack else -1

            if weight_down == -1:
                survivors += 1
            else:
                stack.append(weight_down)
    return survivors + len(stack)

# https://leetcode.com/problems/palindrome-number/

def solution(x: int) -> bool:
    """
    Given an integer x, return true if x is palindrome integer.
    An integer is a palindrome when it reads the same backward as forward. For example,
    121 is palindrome while 123 is not.
    """
    if x < 0:
        return False

    data = []
    while x:
        data.append(x % 10)
        x = x // 10

    return data == data[::-1]

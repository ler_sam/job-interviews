import pytest

from codility.interview.palindrome import solution


@pytest.mark.Palindrome
@pytest.mark.parametrize("input_str, status", [
    ('rnr', True),
    ('rnrd', False),
    ('abcdcba', True)
])
def test_palindrome(input_str, status):
    assert solution(input_str) == status

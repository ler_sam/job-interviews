import pytest

from codility.prefix_sums.min_avg_two_slice import solution


# https://app.codility.com/programmers/lessons/5-prefix_sums/min_avg_two_slice/
@pytest.mark.MinAvgTwoSlice
@pytest.mark.parametrize("A, result", [
    ([4, 2, 2, 5, 1, 5, 8], 1),
])
def test_MinAvgTwoSlice(A, result):
    assert solution(A) == result

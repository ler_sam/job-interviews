import pytest

from codility.counting_elements.missing_integer import solution

# https://app.codility.com/programmers/lessons/4-counting_elements/missing_integer/


@pytest.mark.MissingInteger
@pytest.mark.parametrize("input_array, missing_element", [
    ([1, 3, 6, 4, 1, 2], 5),
    ([1, 2, 3], 4),
    ([-1, -3], 1),
    ([-1, -3, 1, 2, 4, 5], 3),
    ([2,3,4,6,10,1000], 1),
    ([-1, 0, 1, 2, 3, 4, 5, 6, 10, 1000], 7)
])
def test_MissingInteger(input_array, missing_element):
    assert solution(input_array) == missing_element

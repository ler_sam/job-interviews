import pytest

from codility.sorting.distinct import solution

# https://app.codility.com/programmers/lessons/6-sorting/distinct/


@pytest.mark.Distinct
@pytest.mark.parametrize("input_array, result", [
    ([2, 1, 1, 2, 3, 1], 3),
])
def test_distinct(input_array, result):
    assert solution(input_array) == result

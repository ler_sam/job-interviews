import pytest

from codility.arrays.calculate_time_window import calculate_time_window


@pytest.mark.TimeWindow
@pytest.mark.parametrize("values, window_size, expected", [
    ([0.1, 0.5, 1.2, 1.8, 2.5, 3.1, 4.0, 4.5, 5.5, 6.0], 1.0,
     [[0.1, 0.5, 1.2], [1.2, 1.8, 2.5], [2.5, 3.1, 4.0], [4.0, 4.5, 5.5]])
])
def test_time_window(values, window_size, expected):
    actual = calculate_time_window(values, window_size)
    assert actual == expected

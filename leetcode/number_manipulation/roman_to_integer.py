# https://leetcode.com/problems/roman-to-integer/

def solution(s: str) -> int:
    convert_data = {
        'I': 1, 'V': 5, 'X': 10, 'L': 50,
        'C': 100, 'D': 500, 'M': 1000}
    temp = [convert_data[char] for char in s]
    result = 0

    for index in range(len(temp)):
        if index < (len(temp)-1) and temp[index + 1] > temp[index]:
            result -= temp[index]
        else:
            result += temp[index]

    return result

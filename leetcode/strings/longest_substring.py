# https://leetcode.com/problems/longest-substring-without-repeating-characters/

def solution(s: str) -> int:
    """
    3. Longest Substring Without Repeating Characters

    Given a string s, find the length of the longest substring without repeating characters.
    """
    max_length = 0
    lef_index = 0

    buffer = set()

    for char in s:
        if char in buffer:
            if len(buffer) > max_length:
                max_length = len(buffer)
            while char in buffer:
                buffer.remove(s[lef_index])
                lef_index += 1
        buffer.add(char)
    if len(buffer) > max_length:
        max_length = len(buffer)

    return max_length

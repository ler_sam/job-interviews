# https://app.codility.com/programmers/lessons/8-leader/dominator/
def solution(A):
    '''
    Find an index of an array such that its value occurs at more than half of indices in the array.
    https://app.codility.com/demo/results/training8G33Z7-HXE/

    Task Score:     100%
    Correctness:    100%
    Performance:    100%
    '''

    if not A:
        return -1

    temp = A.copy()
    temp.sort()

    n = len(A)
    candidate = temp[n//2]
    leader = -1
    counter = 0
    for i in range(n):
        if A[i] == candidate:
            leader = i
            counter +=1

    if counter > n // 2:
        return leader
    return -1

def solution_91_p(A):
    '''
    Find an index of an array such that its value occurs at more than half of indices in the array.
    https://app.codility.com/demo/results/training2SDSAK-8TJ/

    Task Score:     91%
    Correctness:    87%
    Performance:    100%
    '''
    temp = A.copy()
    temp.sort()

    n = len(A)
    candidate = temp[n//2]
    result = []
    for i in range(n):
        if A[i] == candidate:
            result.append(i)

    if len(result) > n //2:
        return result[0]
    return -1

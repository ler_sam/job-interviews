import pytest

from codility.intersection_sorted import solution


@pytest.mark.intersection
@pytest.mark.parametrize("first_array, second_array, result", [
    ([1, 2, 3, 4, 7, 9], [2, 4, 5, 6, 8, 9], [2, 4, 9]),
])
def test_intersection(first_array, second_array, result):
    assert solution(first_array, second_array) == result

import pytest

from leetcode.arrays.find_difference import solution


@pytest.mark.FindDifference
@pytest.mark.parametrize("actual, expected", [
    ({'s': 'abcd', 't': 'abcde'}, 'e'),
    ({'s': '', 't': 'y'}, 'y'),
    ({'s': 'a', 't': 'aa'}, 'a'),
    ({'s': 'abcd', 't': 'atbdc'}, 't')

])
def test_find_difference(actual, expected):
    assert solution(**actual) == expected

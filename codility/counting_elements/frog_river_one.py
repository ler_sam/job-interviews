# https://app.codility.com/programmers/lessons/4-counting_elements/frog_river_one/

def solution(X, A):
    result = -1
    dict_temp = {}

    for minute, val in enumerate(A):
        if val > X:
            continue

        dict_temp[val] = minute
        if len(dict_temp) == X:
            result = minute
            break

    return result

# https://leetcode.com/problems/valid-sudoku/

def solution(board: list) -> bool:
    """
    Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following
    rules:

    1. Each row must contain the digits 1-9 without repetition.
    2. Each column must contain the digits 1-9 without repetition.
    3. Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.

    Note:
        A Sudoku board (partially filled) could be valid but is not necessarily solvable.
        Only the filled cells need to be validated according to the mentioned rules.

    :type nums: list[list[str]]
    """
    caching_row = {}
    caching_col = {}
    caching_box = {}

    for row in range(len(board[0])):
        for col in range(len(board)):
            curr = board[row][col]
            if curr == '.':
                continue

            if row not in caching_row:
                caching_row[row] = set()

            if col not in caching_col:
                caching_col[col] = set()
            curr_row = row // 3
            curr_col = col // 3
            if (curr_row, curr_col) not in caching_box:
                caching_box[(curr_row, curr_col)] = set()

            if curr in caching_row[row] or curr in caching_col[col] or curr in caching_box[(curr_row, curr_col)]:
                return False

            caching_row[row].add(curr)
            caching_col[col].add(curr)
            caching_box[(curr_row, curr_col)].add(curr)
    return True

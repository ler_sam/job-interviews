# https://app.codility.com/programmers/lessons/5-prefix_sums/genomic_range_query/
# https://en.wikipedia.org/wiki/Range_minimum_query
def solution(S, P, Q):
    """
    Find the minimal nucleotide from a range of sequence DNA.
    https://app.codility.com/demo/results/trainingJJ9TU5-NBR/
    Task Score: 62%
    Correctness: 100%
    Performance: 0%
    """
    convert_dict = {
        'A': 1,
        'C': 2,
        'G': 3,
        'T': 4
    }
    result_array = []

    for p, q in zip(P, Q):
        str_slice = sorted(S[p: q + 1])[0]
        result_array.append(convert_dict[str_slice])

    return result_array

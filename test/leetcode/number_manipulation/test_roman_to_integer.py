import pytest

from leetcode.number_manipulation.roman_to_integer import solution


@pytest.mark.RomanToInteger
@pytest.mark.parametrize("actual, expected", [
    ('III', 3),
    ('IV', 4),
    ('IX', 9),
    ('LVIII', 58),
    ('XII', 12),
    ('XV', 15),
    ('MCMXCIV', 1994),
    ('LXXXIV', 84)
])
def test_roman_to_integer(actual, expected):
    assert solution(actual) == expected

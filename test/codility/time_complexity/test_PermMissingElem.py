import pytest

from codility.time_complexity.perm_missing_elem import solution


@pytest.mark.PermMissingElem
@pytest.mark.parametrize("input_array, missing_number", [
    ([2, 3, 1, 5], 4),
    ([1, 2, 3, 4, 6, 7, 8, 9], 5),
    ([], 1)])
def test_missing_number(input_array, missing_number):
    assert solution(input_array) == missing_number

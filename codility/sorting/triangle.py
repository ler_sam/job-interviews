# https://app.codility.com/programmers/lessons/6-sorting/triangle/

def solution(A):
    """
    Determine whether a triangle can be built from a given set of edges.
    Task Score:     100%
    Correctness:    100%
    Performance:    100%
    """

    def is_triangle(P, Q, R):
        return P + Q > R and Q + R > P and R + P > Q

    A.sort()
    status = 0
    for i in range(len(A) - 2):
        if is_triangle(A[i], A[i + 1], A[i + 2]):
            status = 1
            break

    return status

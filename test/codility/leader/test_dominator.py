import pytest

# https://app.codility.com/programmers/lessons/8-leader/dominator/

from codility.leader.dominator import solution


@pytest.mark.Dominator
@pytest.mark.parametrize("A, result", [
    ([3, 4, 3, 2, 3, -1, 3, 3], 7),
    ([100, 100, 100, 100, 100, 100], 5),
    ([100, -1000, 100, -1000, -1000], 4),
    ([1, 2, 3], -1),
    ([1000000000], 0),
    ([1000, 1], -1),
    ([0, 0], 1),
    ([], -1),
    ([1], 0)
])
def test_dominator(A, result):
    assert solution(A) == result

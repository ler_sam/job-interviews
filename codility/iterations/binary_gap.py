# https://app.codility.com/programmers/lessons/1-iterations/binary_gap/

def solution(N):
    max_theros = 0
    input_bin = f"{N:b}"
    input_list = input_bin.split('1')
    del (input_list[0], input_list[-1])
    if input_list:
        max_theros = max(len(x) for x in input_list)

    return max_theros

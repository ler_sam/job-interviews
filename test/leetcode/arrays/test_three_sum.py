import pytest

from leetcode.arrays.three_sum import solution


@pytest.mark.ThreeSum
@pytest.mark.parametrize("actual, expected", [
    ([-1, 0, 1, 2, -1, -4], [[-1, -1, 2], [-1, 0, 1]]),
    ([], []),
    ([0], []),
])
def test_three_sum(actual, expected):
    assert solution(actual) == expected

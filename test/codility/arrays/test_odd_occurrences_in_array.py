# https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/
import pytest

from codility.arrays.odd_occurrences_in_array import solution


@pytest.mark.OddOccurrencesInArray
@pytest.mark.parametrize("input_array, occurrences", [
    ([9, 3, 9, 3, 9, 7, 9], 7),
    ([1, 5, 3, 4, 5, 3, 1], 4),
    ([4, 1, 2, 1, 7, 4, 6, 2, 7], 6),
])
def test_odd_occurrences_in_array(input_array, occurrences):
    assert solution(input_array) == occurrences
